# Cartel de FLISOL Tenerife 2022

* README.md: Este fichero.

## Ficheros fuente
* BBBFlisolTenerife22.svg: Pantalla con la clave WIFI para Big Blue Button.
* FaltaUnaSemana.svg:      Cartel simple de Falta una semana.
* flisol-tenerife-2022-banner2.xcf: Banner Web 1500x500 con diseño cartel del flisol tenerife 2022.
* flisol-tenerife-2022-escala4.xcf: Cuadrado 512x512 con diseño cartel del flisol tenerife 2022.
* main-flisol-tenerife-2022.xcf: Cuadrado 128x128 con 8 capas de flechas y montaje del cartel del flisol tenerife 2022.
* flisol-tenerife-2022-avatar.xcf: Avatar en 200x200 con diseño flisol tenerife 2022.

## Imágenes generadas
* BBBFlisolTenerife22.png: Wifi 
* flisol-arrow-left.png: Flecha izquierda para llegar a la sede del FLISOL Tenerife 2022.
* flisol-arrow-right.png: Flecha derecha para llegar a la sede del FLISOL Tenerife 2022.
* flisol-tenerife-2022-avatar.png: Avatar del flisol
* flisol-tenerife-2022-banner2.png: Banner
* flisol-tenerife-2022-escala4.png: Cartel cuadrado grande
* main-flisol-tenerife-2022.png: Cartel principal del flisol tenerife 2022


