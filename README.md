# REPOSITORIO FLISOL TENERIFE 2022
Este repositorio alberga toda la documentación del FLISoL Tenerife 2022

La organización está trabajando para estructurar y compartir todos los documentos en este repositorio gitlab.

## Cartel:   
El cartel es obra de David Vargas.  
![](https://gitlab.com/osl-ull/flisol-tenerife/2022/-/raw/main/Cartel/flisol-tenerife-2022-escala4.png)

## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2022/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>
