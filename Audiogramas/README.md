# FLISOL TENERIFE 2022

## Repositorio de los Audiogramas.

Este repositorio alberga los recursos para la elaboración de audiogramas para el FLISoL_Tenerife_2021.

Se basa en el [cartel](https://gitlab.com/osl-ull/flisol-tenerife/2022/-/blob/main/Cartel/flisol-tenerife-2022-escala4.png) al que se le ha añadido un espacio para la foto perfilada sin fondo del ponente del audiograma en un círculo así como el audio del resumen elegido. Para obtener el video se recomienda usar las opciones del comando que se incluye como ejemplo, dónde audio.ogg es el fichero de audio e imagen.png el fichero de la imagen que se pondrá de fondo. El comando se ha pensado para trabajar en 1920x1080 de modo que la imagen debe tener esas dimensiones.

## Comando con **ffmpeg** Para realizar los audiogramas:
`ffmpeg -i audio.flac -i imagen.png -filter_complex "[0:a]showwaves=s=1280x200:mode=cline:colors=white[sw];[1][sw]overlay=x=W-w-0:y=550:format=auto,format=yuv420p[v]" -map "[v]" -map 0:a -movflags +faststart video.mp4 -y`  


## Métodos de contacto:
+ Web: <https://flisol.info/FLISOL2022/Espana/Tenerife>
+ Correo: [flisoltenerife@disroot.org](mailto:flisoltenerife@disroot.org)
+ Mastodon: <https://txs.es/@flisoltenerife>
+ Peertube: <https://tuvideo.encanarias.info/accounts/flisoltenerife>
+ Archive.org: <https://archive.org/details/@flisoltenerife>
+ Twitter: <https://twitter.com/flisoltenerife>
+ Grupo Telegram: <https://t.me/flisoltenerife19/>
 
